function handleCommand(e) {
	if(e.code !== 'Enter') return
	if(this.value === '') return

	// Pop the command
	const command = this.value
	this.value = ''

	// Insert the command
	const paragraph = document.createElement('p')
	const errorMsg = `<span class="err">[ERR]</span> command not found: ${command}`
	paragraph.innerHTML = `<span class="pre"> root@localhost ~ $</span> ${errorMsg}`

	const terminal = document.querySelector('.terminal-wrapper .terminal-content')
	terminal.insertBefore(paragraph, terminal.lastChild)
}

function inputHandler() {
	const input = document.querySelector('.terminal-wrapper .terminal-input input')
	input.addEventListener('keyup', handleCommand)
}

function inputFocus() {
	const input = document.querySelector('.terminal-wrapper .terminal-input input')
	input.focus()
	input.addEventListener('blur', () => input.focus())
}

module.exports = {
	inputHandler,
	inputFocus
}
