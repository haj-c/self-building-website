const terminalStream = [
	{ text: 'Initiating terminal' },
	{ function: 'dotAnimation', iterations: 5 },
	{ text: 'Terminal interface completed.' },
	{ text: 'Downloading Cobalt2 theme' },
	{ function: 'dotAnimation', iterations: 8 },
	{ text: 'Applying Cobalt2 theme.' },
	{ function: 'applyTheme' },
	{ text: 'Make me a sandwich.' },
	{ text: 'no.' },
	{ text: 'sudo Make me a sandwich.' },
	{ text: 'ok. 🍞' },
	{ text: 'Installing graphical interface' },
	{ function: 'dotAnimation', iterations: 6 },
	{ text: 'Initiating graphical interface.' },
	{ function: 'loadPage ' }
]

module.exports = terminalStream
