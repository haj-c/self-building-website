const terminalStream = require('./terminalStream.js')
const iOS = require('../helperFunctions/ios.js')
const { inputHandler, inputFocus } = require('./input.js')
const { dotAnimation, applyTheme, loadPage } = require('./functions.js')
const { printRow } = require('./typing.js')


function readRowOfTerminalStream(index, rows) {
	if(index === rows.length) {
		return
	}

	const row = rows[index]
	if(row.text) {
		setTimeout(() => printRow(row, index + 1, rows), 400)
	} else if(row.function) {
		if(row.function === 'dotAnimation') {
			dotAnimation(0, row.iterations, index + 1, rows)
		} else if(row.function === 'applyTheme') {
			applyTheme(index + 1, rows)
		} else if(row.function === 'loadPage') {
			setTimeout(loadPage, 1200)
		}
	}
}

function init() {
	if(!iOS()) {
		inputFocus()
		inputHandler()
	}
	readRowOfTerminalStream(0, terminalStream)
}

const terminal = { init, readRowOfTerminalStream }

module.exports = terminal
