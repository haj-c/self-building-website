const readRowOfTerminalStream = require('./main').readRowOfTerminalStream

function dotAnimation(index, iterations, rowsIndex, rows) {
	if(index === (iterations * 4) + 1) {
		readRowOfTerminalStream(rowsIndex, rows)
		return
	}

	const paragraphs = document.querySelectorAll('.terminal-wrapper .terminal-content p')
	const paragraph = paragraphs[paragraphs.length - 1]

	if(paragraph.innerHTML.endsWith('...')) {
		paragraph.innerHTML = paragraph.innerHTML.substring(0, paragraph.innerHTML.length - 3)
	} else {
		paragraph.innerHTML += '.'
	}

	setTimeout(() => dotAnimation(index + 1, iterations, rowsIndex, rows), 110)
}

function applyTheme(index, rows) {
	const terminalWrapper = document.querySelector('.terminal-wrapper')
	terminalWrapper.classList.add('cobalt2')

	readRowOfTerminalStream(index, rows)
}

function loadPage() {
	const terminalWrapper = document.querySelector('.terminal-wrapper')
	terminalWrapper.classList.add('remove')

	function removeTerminal(e) {
		if(e.propertyName === 'transform') {
			this.parentElement.removeChild(this)
		}
	}

	terminalWrapper.addEventListener('transitionend', removeTerminal)
}

module.exports = {
	dotAnimation,
	applyTheme,
	loadPage
}
