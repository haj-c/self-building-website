const readRowOfTerminalStream = require('./main').readRowOfTerminalStream

function printCharacter(index, row, rowsIndex, rows) {
	if(index === row.length) {
		readRowOfTerminalStream(rowsIndex, rows)
		return
	}

	const paragraphs = document.querySelectorAll('.terminal-wrapper .terminal-content p')
	const paragraph = paragraphs[paragraphs.length - 1]

	paragraph.innerHTML += row[index]
	setTimeout(() => printCharacter(index + 1, row, rowsIndex, rows), 30)
}

function printRow(row, rowsIndex, rows) {
	const characters = Array.of(...row.text)

	const paragraph = document.createElement('p')
	paragraph.innerHTML = '<span class="pre"> root@localhost ~ $</span> '

	const terminal = document.querySelector('.terminal-wrapper .terminal-content')
	terminal.appendChild(paragraph)

	printCharacter(0, characters, rowsIndex, rows)
}

module.exports = { printRow }
