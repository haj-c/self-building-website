# Self Building Website
A self building website displaying our website building skills.

## Installation
1. Copy the files to the current folder.
2. Globaly install bower and gulp with `npm install -g bower gulp`.
3. Install all bower components with `bower install`.
4. Install all node modules with `npm install`.
5. Run server with `gulp`.