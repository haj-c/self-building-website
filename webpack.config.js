const webpack = require('webpack')
const path = require('path')

module.exports = {
	entry: {
		'main': [
			path.resolve(__dirname, 'src/js/entry.js')
		],
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: '[name].js'
	},
	module: {
		preLoaders: [
			{
				test: /\.js?$/,
				exlude: /node_modules/,
				loader: 'eslint'
			}
		],
		loaders: [
			{
				test: /\.js?$/,
				exlude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015']
				}
			}
		],
	}
}
