const gulp = require('gulp')
const pump = require('pump')
const concat = require('gulp-concat')
const runSequence = require('run-sequence')

const environments = require('gulp-environments')
const development = environments.development
const production = environments.production

const webpack = require('webpack-stream')
const uglify = require('gulp-uglify')

const sass = require('gulp-sass')
const cleanCSS = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')

const pug = require('gulp-pug')

const nodemon = require('gulp-nodemon')
const Cache = require('gulp-file-cache')
const cache = new Cache()

gulp.task('es6', function(err) {
	pump([
		gulp.src('src/js/*.js'),
		webpack(require('./webpack.config.js')),
		production(uglify()),
		gulp.dest('public/js')
	], err)
})

gulp.task('styles', function(err) {
	pump([
		gulp.src('src/sass/style.scss'),
		cache.filter(),
		sass(),
		autoprefixer(),
		gulp.dest('public_dev/css'),
		production(cleanCSS()),
		cache.cache(),
		gulp.dest('public/css')
	], err)
})

gulp.task('pug', function(err) {
	pump([
		gulp.src('src/pug/index.pug'),
		cache.filter(),
		pug(),
		cache.cache(),
		gulp.dest('public/html')
	], err)
})

gulp.task('stream', function(err) {
	const stream = nodemon({
		script: 'app.js',
		ext: 'js scss pug',
		ignore: ['node_modules', 'public', 'public_dev'],
		tasks: ['es6', 'styles', 'pug']
	})
		.on('restart', function () {
			console.log('restarted!')
		})
		.on('crash', function() {
			console.error('Application has crashed!\n')
			stream.emit('restart', 10)  // restart the server in 10 seconds 
		})

	return stream
})

gulp.task('devEnv', () => {
	environments.current(development)
	process.env.NODE_ENV = 'development'
})
gulp.task('proEnv', () => {
	environments.current(production)
	process.env.NODE_ENV = 'production'
})

gulp.task('development', () => { runSequence('devEnv', ['es6', 'styles', 'pug', 'stream']) })
gulp.task('production', () => { runSequence('proEnv', ['es6', 'styles', 'pug', 'stream']) })
gulp.task('build', ['es6', 'styles', 'pug'])

gulp.task('default', ['development'])
